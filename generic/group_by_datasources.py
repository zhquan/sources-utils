#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors:
#     Luis Cañas-Díaz <lcanas@bitergia.com>
#

import argparse
import json
import logging
import sys

DESC_MSG = "Print via stdout all the repos found in the JSON file for a given backend, one per line"

def main():
    configure_logging()
    args = parse_arguments()
    json_file = args.json_file

    try:
        with open(json_file, 'r') as myfile:
            data = json.load(myfile)
    except FileNotFoundError:
        logging.error('JSON file not found')

    if args.show:
        bn = extract_backend_names(data)
        for b in bn:
            print(b)
    else:
        repos = extract_repos_by_backend(data, args.backend)
        if len(repos) == 0:
            logging.info('No results found buddy')
        else:
            for r in repos:
                print(r)

def configure_logging():
    logging.basicConfig(level=logging.INFO,
                        format="[%(asctime)s - %(levelname)s] %(message)s")
    logging.getLogger('requests').setLevel(logging.WARNING)
    logging.getLogger('urrlib3').setLevel(logging.WARNING)

def extract_repos_by_backend(data, backend_name):
    """Return list of repos found for the parameter backend_name"""
    repos = []
    for project in data.keys():
        if backend_name in data[project].keys():
            repos = repos + data[project][backend_name]
    return repos

def extract_backend_names(data):
    """Return list of data sources / backend names found in data"""
    names = []
    for project in data.keys():
        names = names + list(data[project].keys())
    myset = set(names)
    unique_names = list(myset)
    unique_names.sort()
    return unique_names

def parse_arguments():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=DESC_MSG)

    parser.add_argument('json_file', action='store', help='input JSON file')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--backend', action='store', required=False, help='backend name')
    group.add_argument('--show', action='store_true', help='shows available data sources')

    args = parser.parse_args()

    return args

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        s = "\n\nReceived Ctrl-C or other break signal. Exiting.\n"
        sys.stdout.write(s)
        sys.exit(0)
    except RuntimeError as e:
        s = "Error: %s\n" % str(e)
        sys.stderr.write(s)
        sys.exit(1)
